// intersects.cc

#include "intersects.hh"

#include <cassert>

#include "geom.hh"

// reference implementation: https://en.wikipedia.org/wiki/Cohen%E2%80%93Sutherland_algorithm
// license: CC-BY-SA 3.0

enum {
	OUTCODE_LEFT   = 0b0001,
	OUTCODE_RIGHT  = 0b0010,
	OUTCODE_TOP    = 0b0100,
	OUTCODE_BOTTOM = 0b1000
};

unsigned int compute_outcode(const NEWRECT rect, float x, float y) {
	unsigned int outcode = 0;
	if (x < rect.x1) {
		outcode |= OUTCODE_LEFT;
	} else if (x > rect.x2) {
		outcode |= OUTCODE_RIGHT;
	}
	if (y < rect.y1) {
		outcode |= OUTCODE_TOP;
	} else if (y > rect.y2) {
		outcode |= OUTCODE_BOTTOM;
	}
	return outcode;
}

bool intersect_rect_and_line(const NEWRECT rect, float *X1, float *Y1, float *X2, float *Y2) {
	assert(X1 != nullptr && X2 != nullptr && Y1 != nullptr && Y2 != nullptr);

	float x1 = *X1;
	float y1 = *Y1;
	float x2 = *X2;
	float y2 = *Y2;
	unsigned int outcode1 = compute_outcode(rect, x1, y1);
	unsigned int outcode2 = compute_outcode(rect, x2, y2);

	while (true) {
		if (!(outcode1 | outcode2)) {
			break;
		} else if (outcode1 & outcode2) {
			return false;
		} else {
			float x = 0.0f;
			float y = 0.0f;
			unsigned int outcode_out = outcode2 > outcode1 ? outcode2 : outcode1;
			if (outcode_out & OUTCODE_BOTTOM) {
				x = x1 + (x2 - x1) * (rect.y2 - y1) / (y2 - y1);
				y = rect.y2;
			} else if (outcode_out & OUTCODE_TOP) {
				x = x1 + (x2 - x1) * (rect.y1 - y1) / (y2 - y1);
				y = rect.y1;
			} else if (outcode_out & OUTCODE_RIGHT) {
				y = y1 + (y2 - y1) * (rect.x2 - x1) / (x2 - x1);
				x = rect.x2;
			} else if (outcode_out & OUTCODE_LEFT) {
				y = y1 + (y2 - y1) * (rect.x1 - x1) / (x2 - x1);
				x = rect.x1;
			}
			if (outcode_out == outcode1) {
				x1 = x;
				y1 = y;
				outcode1 = compute_outcode(rect, x1, y1);
			} else {
				x2 = x;
				y2 = y;
				outcode2 = compute_outcode(rect, x2, y2);
			}
		}
	}

	*X1 = x1;
	*Y1 = y1;
	*X2 = x2;
	*Y2 = y2;
	return true;
}

bool intersect_rects(const NEWRECT rectA, const NEWRECT rectB) {
	return rectA.x1 < rectB.x2 && rectA.x2 > rectB.x1 && rectA.y1 < rectB.y2 && rectA.y2 > rectB.y1;
}
