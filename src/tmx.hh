// tmx.hh

#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>

#include "map.hh"

struct TmxProperty {
	enum Type { STRING, INT, FLOAT, BOOL, OBJECT };
	Type type;
	union {
		int value_int;
		unsigned int value_uint;
		float value_float;
		bool value_bool;
	};
	std::string value_string;
};

struct TmxProperties {
	std::map<std::string, TmxProperty> m;

	inline TmxProperties() {}
	inline TmxProperties(const TmxProperties &o) : m(o.m) {}
	inline TmxProperties &operator=(const TmxProperties o) {
		this->m = o.m;
		return *this;
	}
	inline TmxProperties(const std::map<std::string, TmxProperty> &map) : m(map) {}

	inline void merge(const TmxProperties &back) {
		this->m.insert(back.m.cbegin(), back.m.cend());
	}

	inline bool has_key_of_type(const char *key, TmxProperty::Type type) const {
		auto it = this->m.find(key);
		return it != this->m.end() && it->second.type == type;
	}
	// strings
	inline bool has_string(const char *key) const { return this->has_key_of_type(key, TmxProperty::Type::STRING); }
	inline const char *get_string(const char *key, const char *def = "") const { return this->has_string(key) ? this->m.at(key).value_string.c_str() : def; }
	// ints
	inline bool has_int(const char *key) const { return this->has_key_of_type(key, TmxProperty::Type::INT); }
	inline int get_int(const char *key, int def = 0) const { return this->has_int(key) ? this->m.at(key).value_int : def; }
	// floats
	inline bool has_float(const char *key) const { return this->has_key_of_type(key, TmxProperty::Type::FLOAT); }
	inline float get_float(const char *key, float def = 0.0f) const { return this->has_float(key) ? this->m.at(key).value_float : def; }
	// bools
	inline bool has_bool(const char *key) const { return this->has_key_of_type(key, TmxProperty::Type::BOOL); }
	inline bool get_bool(const char *key, bool def = false) const { return this->has_bool(key) ? this->m.at(key).value_bool : def; }
	// objects
	inline bool has_object(const char *key) const { return this->has_key_of_type(key, TmxProperty::Type::OBJECT); }
	inline unsigned int get_object(const char *key) const { return this->has_object(key) ? this->m.at(key).value_uint : 0u; }
};

struct TmxLayer {
	enum Type { INVALID, TILE_LAYER, OBJECT_LAYER, GROUP_LAYER };
	Type type = TmxLayer::INVALID;
	unsigned int id = 0;
	float offset_x = 0.0f;
	float offset_y = 0.0f;

	// necessary for subclass's destructors to work in STL containers
	virtual ~TmxLayer() = default;
};
struct TmxGroupLayer : TmxLayer {
	std::vector<std::unique_ptr<TmxLayer>> layers;
};
struct TmxPolyPoints {
	std::vector<std::pair<float, float>> points;
};
struct TmxObject {
	unsigned int id;
	enum Type { RECTANGLE, ELLIPSE, POINT, POLYGON, POLYLINE };
	Type type = TmxObject::RECTANGLE;
	TmxProperties properties;
	float x = 0.0f; /* used by all types */
	float y = 0.0f; /* used by all types */
	float width = 0.0f; /* used by RECTANGLE and ELLIPSE */
	float height = 0.0f; /* used by RECTANGLE and ELLIPSE */
	bool has_gid = false; /* used by RECTANGLE */
	unsigned int gid = 0; /* used by RECTANGLE */
	TmxPolyPoints polypoints; /* used by POLYGON and POLYLINE */
};
struct TmxObjectLayer : TmxLayer {
	ObjectLayerDrawOrder draworder;
	std::vector<TmxObject> objects;
};
struct TmxTileLayer : TmxLayer {
	int tile_offset_x = 0; /* in tiles */
	int tile_offset_y = 0; /* in tiles */
	unsigned int width = 0; /* in tiles */
	unsigned int height = 0; /* in tiles */
	std::vector<unsigned int> tile_gids; /* by gid */
};
struct TmxAnimationFrame {
	unsigned int lid = 0;
	unsigned int duration = 0; /* in ms */
};
struct TmxTile {
	TmxProperties properties;
	unsigned int lid = 0;
	int src_x = 0; /* top left origin */
	int src_y = 0; /* top left origin */
	int width = 0;
	int height = 0;
	TmxObjectLayer collision;
	std::vector<TmxAnimationFrame> animation;
};
struct TmxTileset {
	unsigned int firstgid = 0;
	unsigned int tilecount = 0;
	std::string image;
	std::vector<TmxTile> tiles; /* by lid */
};
struct TmxMap {
	TmxProperties properties;
	int width = 0; /* in tiles */
	int height = 0; /* in tiles */
	int tilewidth = 0; /* in pixels */
	int tileheight = 0; /* in pixels */
	TileLayerRenderOrder renderorder = TileLayerRenderOrder::RIGHT_DOWN;
	SDL_Color background_color;
	std::vector<TmxTileset> tilesets;
	std::vector<std::unique_ptr<TmxLayer>> layers;
};

bool parse_map(TmxMap *map, const char *filename);
