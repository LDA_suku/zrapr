// fs.cc

#include "fs.hh"

#include <algorithm>
#include <string_view>
#include <string>
#include <vector>

#include <physfs.h>
#include <SDL.h>
#include <tinyxml2.h>

#include "gfx.hh"
#include "log.hh"

void *alloc_malloc(PHYSFS_uint64 s) {
	return SDL_malloc(s);
}
void *alloc_realloc(void *ptr, PHYSFS_uint64 s) {
	return SDL_realloc(ptr, s);
}

void fs_init(const char *argv0) {
	// init physfs' allocator
	PHYSFS_Allocator alloc;
	alloc.Init = nullptr;
	alloc.Deinit = nullptr;
	alloc.Malloc = alloc_malloc;
	alloc.Realloc = alloc_realloc;
	alloc.Free = SDL_free;
	PHYSFS_setAllocator(&alloc); // physfs memcpys this into its own memory

	// init physfs
	PHYSFS_permitSymbolicLinks(true);
	PHYSFS_init(argv0);
	const char *prefdir = fs_get_pref_dir();
	PHYSFS_setWriteDir(prefdir);
	PHYSFS_mount(prefdir, nullptr, 1);
	std::string resdir;
	resdir.reserve(256);
	resdir += fs_get_base_dir();
	resdir += "/res";
	PHYSFS_mount(resdir.c_str(), nullptr, 1);

	// init gamecontrollerdb
	PHYSFS_File *gcdbfile = PHYSFS_openRead("gamecontrollerdb.txt");
	if (gcdbfile != nullptr) {
		PHYSFS_sint64 length = PHYSFS_fileLength(gcdbfile);
		char *mem = (char *)SDL_malloc(length);
		PHYSFS_readBytes(gcdbfile, mem, length);
		PHYSFS_close(gcdbfile);
		SDL_RWops *rw = SDL_RWFromConstMem(mem, length);
		SDL_GameControllerAddMappingsFromRW(rw, false);
		SDL_RWclose(rw);
		SDL_free(mem);
	}
}

void fs_uninit() {
	PHYSFS_deinit();
}

const char *fs_get_pref_dir() {
	return PHYSFS_getPrefDir(".", "zrapr");
}
const char *fs_get_base_dir() {
	return PHYSFS_getBaseDir();
}

std::vector<DirEntry> fs_enumerate(const char *path) {
	std::vector<DirEntry> dir_entries;

	char **files_list = PHYSFS_enumerateFiles(path);
	PHYSFS_Stat stat;

	for (char **file = files_list; *file != nullptr; file++) {
		std::string subpath;
		subpath.reserve(256);
		subpath += path;
		subpath += '/';
		subpath += *file;
		if (PHYSFS_stat(subpath.c_str(), &stat)) {
			DirEntry &dir_entry = dir_entries.emplace_back();
			dir_entry.name = *file;
			dir_entry.stat = stat;
		}
	}

	return dir_entries;
}

tinyxml2::XMLError fs_load_xml(const char *path, tinyxml2::XMLDocument *doc) {
	PHYSFS_File *handle = PHYSFS_openRead(path);
	if (handle == nullptr) {
		return tinyxml2::XML_ERROR_FILE_NOT_FOUND;
	}

	PHYSFS_sint64 length = PHYSFS_fileLength(handle);
	char *mem = (char *)SDL_malloc(length);
	PHYSFS_readBytes(handle, mem, length);
	PHYSFS_close(handle);

	tinyxml2::XMLError err = doc->Parse(mem, length);
	SDL_free(mem);
	return err;
}

int fs_load_ini(const char *path, ini_handler reader, void *user) {
	PHYSFS_File *handle = PHYSFS_openRead(path);
	if (handle == nullptr) {
		return 0;
	}

	PHYSFS_sint64 length = PHYSFS_fileLength(handle);
	char *mem = (char *)SDL_malloc(length + 1);
	PHYSFS_readBytes(handle, mem, length);
	PHYSFS_close(handle);
	mem[length] = '\0';

	int r = ini_parse_string(mem, reader, user);
	SDL_free(mem);
	return r;
}

void create_tiled_project() {
	std::string prefdir = fs_get_pref_dir();
	size_t idx = 0;
	for (;;) {
		idx = prefdir.find_first_of("\"\\", idx);
		if (idx == std::string::npos) break;
		prefdir.insert(idx, 1, '\\');
		idx += 2;
	}

	std::string project_path;
	project_path.reserve(256);
	project_path += PHYSFS_getBaseDir();
	project_path += "/zrapr.tiled-project";

	SDL_RWops *rw = SDL_RWFromFile(project_path.c_str(), "w");
	const char *json =
		"{\n"
		"  \"automappingRulesFile\": \"\",\n"
		"  \"commands\": [],\n"
		"  \"extensionsPath\": \"extensions\",\n"
		"  \"folders\": [\n"
		"    \"res\",\n" // (basedir)/res
		"    \"%s\"\n" // (prefdir)
		"  ],\n"
		"  \"objectTypesFile\": \"res/objecttypes.xml\"\n"
		"}\n";
	static char buf[512];
	int bufsize = snprintf(buf, sizeof buf, json, prefdir.c_str());
	SDL_RWwrite(rw, buf, 1, bufsize);
	SDL_RWclose(rw);
}
