// gfx.hh

#pragma once

#include "gfx.hh"

#include <SDL.h>

#include "geom.hh"

struct ZrTexture;

extern void gfx_init();
extern void gfx_uninit();

extern void gfx_set_backbuffer_size(int w, int h);

extern void gfx_flush();
extern void gfx_begin_frame();
extern void gfx_end_frame();
extern void gfx_transform_coordinates(float x, float y, float *xp, float *yp);

extern ZrTexture *gfx_create_texture(unsigned int width, unsigned int height, void *pixels = nullptr);
extern void gfx_upload_texture(ZrTexture *texture, int x, int y, int width, int height, void *pixels);
extern ZrTexture *gfx_load_texture_from_file(const char *path);
extern void gfx_get_texture_dimensions(ZrTexture *texture, unsigned int *x, unsigned int *y);
extern void *gfx_get_texture_handle(ZrTexture *texture);
extern void gfx_free_texture(ZrTexture *texture);

extern void gfx_clear(SDL_Color color);
extern void gfx_draw_line(const SDL_Color color, float x1, float y1, float x2, float y2);
extern void gfx_draw_rect(const SDL_Color color, const NEWRECT rect);
extern void gfx_draw_rect_filled(const SDL_Color color, const NEWRECT rect);
extern void gfx_draw_texture(ZrTexture *texture, const NEWRECT srcrect, const NEWRECT dstrect);
