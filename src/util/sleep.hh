// sleep.hh

#pragma once

enum SleepMethodEnum {
	SDL_DELAY,
	CXX11_THREAD,
	POSIX_NANOSLEEP,
};

extern SleepMethodEnum g_sleep_method;
extern bool g_sleep_correction;
extern float g_sleep_threshold;

extern void sleep(float secs);

extern const char *sleepmethod_to_string(SleepMethodEnum m);
extern bool sleepmethod_from_string(const char *str, SleepMethodEnum *out);
