// csv.cc

#include "csv.hh"

#include <cstdlib>
#include <cstring>

#include <vector>

std::vector<unsigned int> csv_decode(const char *encoded_data) {
	// assume the decoded data will be 200% the size of the encoded data;
	std::vector<unsigned int> out_ints;
	out_ints.reserve(strlen(encoded_data) * 2 / sizeof(unsigned int));

	const char *str = encoded_data;
	while (true) {
		while (*str == ' ' || *str == '\n') str++;
		unsigned int v = strtoul(str, (char **)&str, 10);
		out_ints.push_back(v);
		if (*str != ',') break;
		str++;
	}

	return out_ints;
}
