// inflate.cc

#include "inflate.hh"

#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstring>

#include <vector>

#include <zlib.h>
#include <zstd.h>

#include "../log.hh"

std::vector<uint8_t> inflate_zlib(std::vector<uint8_t> compressed_data) {
	std::vector<uint8_t> out;

	static uint8_t buf[4096];
	int err;

	z_stream s;
	memset(&s, 0, sizeof s);
	s.next_in = compressed_data.data();
	s.avail_in = compressed_data.size();
	err = inflateInit2(&s, 15 | 32);
	if (err != Z_OK) {
		zr_log_error("zlib init error: %s", zError(err));
		return std::vector<uint8_t>();
	}

	while (true) {
		s.next_out = buf;
		s.avail_out = sizeof buf;
		err = inflate(&s, Z_SYNC_FLUSH);

		if (err == Z_OK) {
			out.insert(out.end(), buf, buf + sizeof buf - s.avail_out);
		} else if (err == Z_STREAM_END) {
			out.insert(out.end(), buf, buf + sizeof buf - s.avail_out);
			break;
		} else {
			zr_log_error("zlib error: %s", zError(err));
			return std::vector<uint8_t>();
		}
	}
	assert(s.avail_in == 0);

	err = inflateEnd(&s);
	if (err != Z_OK) {
		zr_log_error("zlib end error: %s", zError(err));
		return std::vector<uint8_t>();
	}

	return out;
}

std::vector<uint8_t> inflate_zstd(std::vector<uint8_t> compressed_data) {
	std::vector<uint8_t> out;

	static uint8_t buf[4096];
	size_t err;

	ZSTD_DStream *ds = ZSTD_createDStream();
	err = ZSTD_initDStream(ds);
	if (ZSTD_isError(err)) {
		fprintf(stderr, "zstd init error: %s\n", ZSTD_getErrorName(err));
		return std::vector<uint8_t>();
	}

	ZSTD_inBuffer inbuf;
	inbuf.src = compressed_data.data();
	inbuf.size = compressed_data.size();
	inbuf.pos = 0;
	ZSTD_outBuffer outbuf;
	outbuf.dst = buf;
	outbuf.size = sizeof buf;

	while (inbuf.pos < inbuf.size) {
		outbuf.pos = 0;
		err = ZSTD_decompressStream(ds, &outbuf, &inbuf);
		if (ZSTD_isError(err)) {
			fprintf(stderr, "zstd error: %s\n", ZSTD_getErrorName(err));
			return std::vector<uint8_t>();
		}
		out.insert(out.end(), buf, buf + outbuf.pos);
	}

	err = ZSTD_freeDStream(ds);
	if (ZSTD_isError(err)) {
		fprintf(stderr, "zstd end error: %s\n", ZSTD_getErrorName(err));
		return std::vector<uint8_t>();
	}

	return out;
}
