// fs.hh

#pragma once

#include <string>
#include <vector>

#include <ini.h>
#include <physfs.h>
#include <tinyxml2.h>

#include "gfx.hh"

extern void fs_init(const char *argv0);
extern void fs_uninit();

extern const char *fs_get_pref_dir();
extern const char *fs_get_base_dir();

struct DirEntry {
	std::string name;
	PHYSFS_Stat stat;

	inline bool is_dir() const { return stat.filetype == PHYSFS_FILETYPE_DIRECTORY; }
};

std::vector<DirEntry> fs_enumerate(const char *path);

extern tinyxml2::XMLError fs_load_xml(const char *path, tinyxml2::XMLDocument *doc);
extern int fs_load_ini(const char *path, ini_handler handler, void *user = nullptr);

extern void create_tiled_project();
