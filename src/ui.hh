// ui.hh

#pragma once

#include <entt/entt.hpp>

extern void ui_main_menu_bar(bool *running);
extern void ui_main_menu_bar_library_versions();
extern void ui_ecs_view(bool *open_ptr, entt::registry &reg);
extern void ui_collision_view_control(bool *open_ptr);
extern void ui_input_control(bool *open_ptr);
extern void ui_level_select(bool *open_ptr, entt::registry &reg);
extern void ui_playerchar_select(bool *open_ptr, entt::registry &reg);
extern void ui_log(bool *open_ptr);
