// playerchar.hh

#pragma once

#include <cstdint>
#include <vector>

#include "gfx.hh"

struct PlayerChar {
	std::vector<uint8_t> bitmap;
	int frame_width = 32;
	int image_width = 0;
	int image_height = 0;
	int vertical_offset = 0;
	float animation_time = 0.166667f;
	bool rightfacing = false;
	std::vector<unsigned int> idle = { 0 };
	std::vector<unsigned int> walk = { 1, 0, 2, 0 };
	std::vector<unsigned int> idle_up = { 3 };
	std::vector<unsigned int> walk_up = { 4, 3, 5, 3 };
	std::vector<unsigned int> jump = { 1 };
	std::vector<unsigned int> jump_up = { 4 };
	std::vector<unsigned int> jump_down = { 6 };
	std::vector<unsigned int> fall = { 2 };
	std::vector<unsigned int> fall_up = { 5 };
	std::vector<unsigned int> fall_down = { 6 };
	unsigned int back = 7;
};

extern bool load_playerchar(PlayerChar *pc, const char *path);
extern void set_playerchar(const PlayerChar &pc);
extern void load_and_set_playerchar(const char *path);
