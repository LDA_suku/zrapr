// anno.hh

#pragma once

#if defined(__GNUC__)
# define PRINTF_FORMAT_STRING
# define PRINTF_VARARG_FN(fmtargnumber) __attribute__((format(printf, fmtargnumber, fmtargnumber+1)))
# define VPRINTF_FORMAT_STRING
# define VPRINTF_VARARG_FN(fmtargnumber)
# define SCANF_FORMAT_STRING
# define SCANF_VARARG_FN(fmtargnumber) __attribute__((format(scanf, fmtargnumber, fmtargnumber+1)))
#elif defined(_MSC_VER)
# include <sal.h>
# define PRINTF_FORMAT_STRING _Printf_format_string_
# define PRINTF_VARARG_FN(fmtargnumber)
# define VPRINTF_FORMAT_STRING _Printf_format_string_
# define VPRINTF_VARARG_FN(fmtargnumber)
# define SCANF_FORMAT_STRING _Scanf_format_string_impl_
# define SCANF_VARARG_FN(fmtargnumber)
#else
# define PRINTF_FORMAT_STRING
# define PRINTF_VARARG_FN(fmtargnumber)
# define VPRINTF_FORMAT_STRING
# define VPRINTF_VARARG_FN(fmtargnumber)
# define SCANF_FORMAT_STRING
# define SCANF_VARARG_FN(fmtargnumber)
#endif
