// log.hh

#pragma once

#include <cstdarg>
#include <string>

#include "anno.hh"

enum ZrLogLevel { INFO, WARN, ERROR, DEBUG };

struct ZrLogEntry {
	ZrLogLevel level;
	std::string cat;
	std::string message;
};

extern void zr_log_v(ZrLogLevel level, VPRINTF_FORMAT_STRING const char *fmt, va_list v) VPRINTF_VARARG_FN(2);
extern void zr_log_info_v(VPRINTF_FORMAT_STRING const char *fmt, va_list v) VPRINTF_VARARG_FN(1);
extern void zr_log_warn_v(VPRINTF_FORMAT_STRING const char *fmt, va_list v) VPRINTF_VARARG_FN(1);
extern void zr_log_error_v(VPRINTF_FORMAT_STRING const char *fmt, va_list v) VPRINTF_VARARG_FN(1);
extern void zr_log_debug_v(VPRINTF_FORMAT_STRING const char *fmt, va_list v) VPRINTF_VARARG_FN(1);

extern void zr_log(ZrLogLevel level, VPRINTF_FORMAT_STRING const char *fmt, ...) VPRINTF_VARARG_FN(2);
extern void zr_log_info(PRINTF_FORMAT_STRING const char *fmt, ...) PRINTF_VARARG_FN(1);
extern void zr_log_warn(PRINTF_FORMAT_STRING const char *fmt, ...) PRINTF_VARARG_FN(1);
extern void zr_log_error(PRINTF_FORMAT_STRING const char *fmt, ...) PRINTF_VARARG_FN(1);
extern void zr_log_debug(PRINTF_FORMAT_STRING const char *fmt, ...) PRINTF_VARARG_FN(1);

extern void zr_log_cat_push(const char *cat);
extern void zr_log_cat_pop();
