// configfile.hh

#pragma once

extern void load_config_from_disk();
extern void save_config_to_disk();
extern void config_mark_dirty();
extern void config_tick();
