// entities.cc

#include "entities.hh"

#include <entt/entt.hpp>

#include "comp.hh"
#include "context.hh"
#include "geom.hh"
#include "map.hh"

void entity_add_body_collision_part(entt::registry &reg, entt::entity e, BodyCollisionPart part) {
	CompBodyCollision &bodycol = reg.get_or_emplace<CompBodyCollision>(e);
	bodycol.parts.push_back(part);
}

void entity_add_static_collision_part(entt::registry &reg, entt::entity e, StaticCollisionPart part) {
	CompStaticCollision &bodycol = reg.get_or_emplace<CompStaticCollision>(e);
	bodycol.parts.push_back(part);
}

entt::entity make_demo_player(entt::registry &reg) {
	entt::entity demo_player = reg.create();
	reg.emplace<CompPosition>(demo_player, g_game_context.spawnpoint_x, g_game_context.spawnpoint_y);
	reg.emplace<CompPhysics>(demo_player, 0.0f, 0.0f);
	reg.emplace<CompPlayer>(demo_player);
	entity_add_body_collision_part(reg, demo_player, { .rect = NEWRECT::centered_of_size(24.0f, 32.0f), .solid = true });
	reg.emplace<CompCoinCollector>(demo_player);
	reg.emplace<CompRender>(demo_player);
	g_client_context.camera_entity = demo_player;
	return demo_player;
}

void entity_update_tile_layer_static_collision(entt::registry &reg, entt::entity e) {
	CompTileLayer &tilelayer = reg.get<CompTileLayer>(e);

	CompStaticCollision &staticcollision = reg.get_or_emplace<CompStaticCollision>(e);
	staticcollision.parts.clear();
	for (int y = 0; y < tilelayer.height; y++) {
		for (int x = 0; x < tilelayer.width; x++) {
			unsigned int gid = tilelayer.tile_gids[y * tilelayer.width + x];
			const ZrTile *tile = get_tile(gid);

			float bx = x * tilelayer.tile_width;
			float by = y * tilelayer.tile_height;
			for (StaticCollisionPart statpart : tile->static_collision_absolute) {
				staticcollision.parts.push_back({
					statpart.type,
					bx + statpart.x1,
					by + statpart.y1 + tilelayer.tile_height - tile->srch,
					bx + statpart.x2,
					by + statpart.y2 + tilelayer.tile_height - tile->srch
				});
			}
		}
	}
}

void entity_update_tile_static_collision(entt::registry &reg, entt::entity e) {
	CompTile &tilecomp = reg.get<CompTile>(e);

	unsigned int gid = tilecomp.gid;
	const ZrTile *tile = get_tile(gid);

	if (tile->static_collision_normalized.empty()) return;

	CompStaticCollision &staticcollision = reg.get_or_emplace<CompStaticCollision>(e);
	staticcollision.parts.clear();
	for (const StaticCollisionPart &statpart_normalized : tile->static_collision_normalized) {
		float tilecomp_width = NEWRECT::width(tilecomp.rect);
		float tilecomp_height = NEWRECT::height(tilecomp.rect);

		StaticCollisionPart &statpart = staticcollision.parts.emplace_back();
		statpart.type = statpart_normalized.type;
		statpart.x1 = (statpart_normalized.x1 - 0.5f) * tilecomp_width;
		statpart.y1 = (statpart_normalized.y1 - 0.5f) * tilecomp_height;
		statpart.x2 = (statpart_normalized.x2 - 0.5f) * tilecomp_width;
		statpart.y2 = (statpart_normalized.y2 - 0.5f) * tilecomp_height;
	}
}

void entity_update_tile_body_collision(entt::registry &reg, entt::entity e) {
	CompTile &tilecomp = reg.get<CompTile>(e);

	unsigned int gid = tilecomp.gid;
	const ZrTile *tile = get_tile(gid);

	if (tile->body_collision_normalized.empty()) return;

	CompBodyCollision &bodycollision = reg.get_or_emplace<CompBodyCollision>(e);
	bodycollision.parts.clear();
	for (const BodyCollisionPart &bodypart_normalized : tile->body_collision_normalized) {
		float tilecomp_width = tilecomp.rect.x2 - tilecomp.rect.x1;
		float tilecomp_height = tilecomp.rect.y2 - tilecomp.rect.y1;

		BodyCollisionPart &bodypart = bodycollision.parts.emplace_back();
		bodypart.solid = bodypart_normalized.solid;
		bodypart.rect.x1 = (bodypart_normalized.rect.x1 - 0.5f) * tilecomp_width;
		bodypart.rect.y1 = (bodypart_normalized.rect.y1 - 0.5f) * tilecomp_height;
		bodypart.rect.x2 = (bodypart_normalized.rect.x2 - 0.5f) * tilecomp_width;
		bodypart.rect.y2 = (bodypart_normalized.rect.y2 - 0.5f) * tilecomp_height;
	}
}
